# Тестовое задание: Адресная книга

Порядок развертывания приложения:
```sh
$ composer install
$ ./yii migrate/up
$ php -S 127.0.0.1:9000 -t web/
```

Открываем в браузере адрес: http://127.0.0.1:9000/index.php?r=address-book