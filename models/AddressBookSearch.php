<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AddressBook;

/**
 * AddressBookSearch represents the model behind the search form of `app\models\AddressBook`.
 */
class AddressBookSearch extends AddressBook
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'phone_home', 'phone_work'], 'integer'],
            [['name', 'surname', 'company', 'place', 'email_home', 'email_work'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AddressBook::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'phone_home' => $this->phone_home,
            'phone_work' => $this->phone_work,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'company', $this->company])
            ->andFilterWhere(['like', 'place', $this->place])
            ->andFilterWhere(['like', 'email_home', $this->email_home])
            ->andFilterWhere(['like', 'email_work', $this->email_work]);

        return $dataProvider;
    }
}
