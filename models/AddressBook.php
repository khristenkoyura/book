<?php

namespace app\models;

use app\validators\EitherValidator;
use udokmeci\yii2PhoneValidator\PhoneValidator;
use Yii;

/**
 * This is the model class for table "address_book".
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $company
 * @property string $place
 * @property string $email_home
 * @property string $email_work
 * @property int $phone_home
 * @property int $phone_work
 */
class AddressBook extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address_book';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['phone_home', 'phone_work'], EitherValidator::class],
            [['phone_home', 'phone_work'], PhoneValidator::class, 'country'=> 'RU'],
            [['name', 'surname', 'company', 'place', 'email_home', 'email_work'], 'string', 'max' => 32],
            [['email_home', 'email_work'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'company' => 'Компания',
            'place' => 'Должность',
            'email_home' => 'Email домашний',
            'email_work' => 'Email рабочий',
            'phone_home' => 'Телефон домашний',
            'phone_work' => 'Телефон рабочий',
        ];
    }
}
