<?php

use yii\db\Migration;

class m170131_204327_address_book extends Migration
{
    public function up()
    {
        $this->createTable('address_book', [
            'id' => $this->primaryKey(),
            'name' => $this->string(32),
            'surname' => $this->string(32),
            'company' => $this->string(32),
            'place' => $this->string(32),
            'email_home' => $this->string(32),
            'email_work' => $this->string(32),
            'phone_home' => $this->string(10),
            'phone_work' => $this->string(10),
        ]);
    }

    public function down()
    {
        $this->dropTable('address_book');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
